<%@page contentType="text/html" pageEncoding="UTF-8"
	import="java.sql.*, recommender.service.*, java.util.*"%>
<%
	String username = new String();
	if (session.getAttribute("userInfo") == null) {
		response.sendRedirect("login.jsp");
	} else {
		username = session.getAttribute("userInfo").toString();
	}
	List<Integer> movieId = new ArrayList<Integer>();
	List<String> movieName = new ArrayList<String>();
	List<Integer> clusterId = new ArrayList<Integer>();
	int count = 0;
	Connection conn = DatabaseConnection.getConnection();
	String sql = "select * from movie";
	PreparedStatement pstmt = conn.prepareStatement(sql);
	ResultSet rs = pstmt.executeQuery();
	while (rs.next()) {
		movieId.add(rs.getInt("MovieId"));
		movieName.add(rs.getString("MovieName"));
		clusterId.add(rs.getInt("ClusterId"));
		count++;

	}
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Movie Recommender System Administration</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/js">
        function showDiv() {
                document.getElementById('add').style.display = "block";
             }
		var logout = function () {
	console.log("hello");
		<%session.removeAttribute("userInfo");%>
		}
       </script>
</head>
<body>
	<jsp:include page="header.jsp" />
	<div class="container">
		<div class="admin">
			<div class="row clearfix">
				<div class="col-md-12">
					<div class="admin-name"><%=username%>
						| <a href="login.jsp" onclick="logout()">Log Out</a>
					</div>
					<form name="form"
						action="${pageContext.request.contextPath}/PageController"
						method="post">
						<div class="col-md-3 panel">

							<label class="content">Dashboard</label>
							<p class="content">
								<input type="text" name="q" class="form-control"
									placeholder="Search...">
							</p>
							<p>
								<input type="submit" value="Add" name="add"
									class="btn btn-primary btn-admin">
							</p>
							<p>
								<input type="submit" value="Cluster" name="cluster"
									class="btn btn-primary btn-admin">
							</p>

						</div>
					</form>
					<div class="col-md-9">
						<div class="add">

							<table class="table">
								<tr>
									<th>S.N.</th>
									<th>Movie Id</th>
									<th>Movie Name</th>
									<th>Cluster Id</th>
									<th>Action</th>
								</tr>
								<%
									for (int i = 0; i < count; i++) {
								%>
								<tr>
									<form name="edit-form"
										action="${pageContext.request.contextPath}/MovieController?id=<%=movieId.get(i) %>"
										method="post"></form>
									<td><%=(i + 1)%></td>
									<td><%=movieId.get(i)%></td>
									<td><%=movieName.get(i)%></td>
									<td><%=clusterId.get(i)%></td>
									<td>
										<table>
											<tr>
												<td><span class="label label-primary"><button
															type="submit" name="update">Update</button></span></td>
												<td><span class="label label-primary"><button
															name="delete">Delete</button></span></td>
											</tr>
										</table>
									</td>
								</tr>
								<%
									}
								%>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@include file="footer.jsp"%>
</body>
</html>
