<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Movie Recomendation System</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/js">
        function showDiv() {
                document.getElementById('add').style.display = "block";
             }
       </script>
</head>
<body>
	<%@include file="header.jsp"%>
	<div class="container">
		<div class="admin">
			<div class="row clearfix">
				<div class="col-md-12">
					<div class="add">
						<form name="form"
							action="${pageContext.request.contextPath}/InsertController"
							method="post">
							<h4>Movie Detail</h4>
							<div class="form-group">
								<table cellpadding="5">
									<tr>
										<td>Movie Name:</td>
										<td><input type="text" name="mName" class="form-control"
											placeholder="Movie Name"></td>
									</tr>
									<tr>
										<td>Available Link:</td>
										<td><input type="text" name="avlLink"
											class="form-control" placeholder="Available Link"></td>
									</tr>
									<tr>
										<td>Director Name:</td>
										<td><input type="text" name="dirName"
											class="form-control" placeholder="Director Name"></td>
									</tr>
									<tr>
										<td>Release Date:</td>
										<td><input type="date" name="rDate" class="form-control"
											placeholder="Release Date"></td>
									</tr>
									<tr>
										<td>Running time:</td>
										<td><input type="text" name="time" class="form-control"
											placeholder="Running time"></td>
									</tr>
									<tr>
										<td>Language:</td>
										<td><input type="text" name="language"
											class="form-control" placeholder="Language"></td>
									</tr>
									<tr>
										<td>Budget:</td>
										<td><input type="text" name="budget" class="form-control"
											placeholder="Budget"></td>
									</tr>
									<tr>
										<td>Image:</td>
										<td><input class="btn btn-xs" type="file" name="image"
											accept=".jpg, .jpeg, .png"></td>
									</tr>
									<tr>
										<td>Subtitle File Path:</td>
										<td><input class="btn btn-xs" type="file" name="subpath"
											accept=".srt"></td>
									</tr>
									<tr>
										<td><input type="submit" value="Add" name="add"
											class="btn btn-primary"></td>
										<td><input type="submit" value="Cancel" name="cancel"
											class="btn btn-primary"></td>
									</tr>
								</table>
							</div>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
	<%@include file="footer.jsp"%>
</body>
</html>
