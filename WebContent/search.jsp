<%@page contentType="text/html" pageEncoding="UTF-8"
	import="java.sql.*, recommender.service.*, java.util.*"%>
<%
	Connection con = DatabaseConnection.getConnection();
	String search = request.getParameter("search");
	String sql = "select * from movie where moviename like '"+search+"'";
	PreparedStatement pstmt = con.prepareStatement(sql);
	ResultSet rs = pstmt.executeQuery();
	int clusterid = 0;
	int searchmovieid = 0;
	String searchmovie = new String();
	String searchdirector = new String();
	java.sql.Date searchrelease;
	String searchtime = new String();
	String searchlanguage = new String();
	String searchbudget = new String();
	String searchimage = new String();
	String searchavailablelink = new String();
	int size = 0;
	while(rs.next()){
		searchmovieid = rs.getInt("movieid");
		clusterid = rs.getInt("ClusterId");
		searchmovie = rs.getString("MovieName");
		searchdirector = rs.getString("DirectorName");
		searchrelease = rs.getDate("ReleaseDate");
		searchtime = rs.getString("TotalTime");
		searchlanguage = rs.getString("MLanguage");
		searchbudget = rs.getString("Budget");
		searchimage = rs.getString("ImagePath");
		searchavailablelink = rs.getString("AvailableLink");
		size++;
	}
	int recommend = 0;
	List<String> movieName = new ArrayList<String>();
	List<String> directorName = new ArrayList<String>();
	List<java.sql.Date> releaseDate = new ArrayList<java.sql.Date>();
	List<String> totalTime = new ArrayList<String>();
	List<String> mLanguage = new ArrayList<String>();
	List<String> budget = new ArrayList<String>();
	List<String> image = new ArrayList<String>();
	List<String> availableLink = new ArrayList<String>();
	if(size != 0){
		String sqlSearch = "select * from movie where clusterid = '"+clusterid+"' and movieid != '"+searchmovieid+"'";
		pstmt = con.prepareStatement(sqlSearch);
		ResultSet rs1 = pstmt.executeQuery();
		while(rs1.next()){
	movieName.add(rs1.getString("MovieName"));
	directorName.add(rs1.getString("DirectorName"));
	releaseDate.add(rs1.getDate("ReleaseDate"));
	totalTime.add(rs1.getString("TotalTime"));
	mLanguage.add(rs1.getString("MLanguage"));
	budget.add(rs1.getString("Budget"));
	image.add(rs1.getString("ImagePath"));
	availableLink.add(rs1.getString("AvailableLink"));
	recommend++;
		}
	}else{
		response.sendRedirect("nomovie.jsp");
	}
	
	rs.close();
	pstmt.close();
	con.close();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Movie Recommendation</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?=$titles->description?>">
<meta name="keywords" content="<?=$titles->keywords?>" />
<meta name="author" content="">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="img/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="img/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="img/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="img/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="images/<?=$titles->logo?>">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" src="js/tabs.js"></script>
<script type="text/javascript" src="js/ajax.js"></script>


<script>
	$(document).ready(function() {
		$(".detail").hide();
		$("div.movie").click(function() {
			var id = $(this).attr('id');
			id = 'detail' + id;
			$("#" + id).slideToggle("slow");
		});
	});
</script>

<script>
	$(document).ready(function() {
		$('#search').keyup(function() {
			var valThis = $(this).val().toLowerCase();
			$('.main1>p').each(function() {
				var text = $(this).text().toLowerCase();
				(text.indexOf(valThis) == 0) ? $(this).show() : $(this).hide();
			});
		});
	});
</script>
</head>
<body>
	<%@include file="header.jsp"%>
	<div class="container">

		<div class="main">
			<div class="row clearfix">

				<div class="col-md-2"></div>
				<div class="col-md-8" id="search">
					<div class="search">

						<form name="form" role="form" method="post" action="search.jsp">
							<div class="form-group">
								<input type="text" class="form-control" name="search"
									placeholder="Find your movies...">
							</div>
						</form>
					</div>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="main1">
				<div class="row clearfix">
					<p class="rec">Your recent search</p>
					<div class="col-md-3">
						<div class="movie">
							<img src="images/<%=searchimage%>">
						</div>
					</div>
					<div class="col-md-9">
						<p class="cap" id="searched"><%=searchmovie%></p>
						<p class="cap">
							By:
							<%=searchdirector%>
						<p class="cap">
							Language:
							<%=searchlanguage%>
						<p class="cap">
							Time:
							<%=searchtime%>
						<p class="cap">
							Budget:
							<%=searchbudget%>
						<p class="cap">
							Available on: <a href="http://www.moviebank.com">http://www.moviebank.com</a>
						</p>
					</div>
				</div>

				<%
					if (recommend != 0) {
						int count = 0;
				%>
				<p class="rec">Recommended Movies</p>
				<%
					for (int i = 0; i < movieName.size(); i++) {
							if (count >= 5)
								break;
				%>
				<div class="row clearfix">
					<div class="col-md-3">
						<div class="movie" id="<%=i%>">
							<img src="images/<%=image.get(i)%>">
						</div>
					</div>
					<div class="col-md-9">
						<p class="cap">
							<a href="search.jsp" class="a"><%=movieName.get(i)%></a>
						</p>
						<p class="cap">
							By
							<%=directorName.get(i)%>
						<p class="cap">
							Available on: <a href="<%=availableLink.get(i)%>"><%=availableLink.get(i)%></a>
						</p>
					</div>
				</div>

				<div class="row clearfix">
					<div class="detail" id="detail<%=i%>">
						<p class="rec">
							Detail of
							<%=movieName.get(i)%></p>
						<div class="col-md-4">

							<img src="images/<%=image.get(i)%>">



						</div>

						<div class="col-md-5">


							<p class="cap">
								By :<%=directorName.get(i)%>
							<p class="cap">
								Release Date:
								<%=releaseDate.get(i)%></p>
							<p class="cap">
								Total time:
								<%=totalTime.get(i)%></p>
							<p class="cap">
								Language:
								<%=mLanguage.get(i)%></p>
							<p class="cap">
								Budget:
								<%=budget.get(i)%></p>
							<p class="cap">
								Available on : <a href="<%=availableLink.get(i)%>"><%=availableLink.get(i)%></a>
							</p>

						</div>
					</div>
				</div>


				<%
					count++;
						}
				%>

				<%
					}
				%>


			</div>
		</div>
	</div>
	<%@include file="footer.jsp"%>
</body>
</html>
