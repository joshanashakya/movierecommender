<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"
	import="java.sql.*, recommender.service.*, java.util.*"%>
<%
	Object userInfo = session.getAttribute("userInfo");
	if (userInfo == null) {
		response.sendRedirect("login.jsp");

	}
	String username = userInfo.toString();
	Connection conn = DatabaseConnection.getConnection();
	String sql = "select * from cluster";
	PreparedStatement pstmt = conn.prepareStatement(sql);
	ResultSet rs = pstmt.executeQuery();
	Map<Integer, String> cluster = new HashMap<Integer, String>();
	while (rs.next()) {
		cluster.put(rs.getInt("ClusterId"), rs.getString("ClusterName"));
	}
	Map<Integer, Integer> movies = new HashMap<Integer, Integer>();
	for (Integer i : cluster.keySet()) {
		String sql1 = "select movieId from movie where ClusterId = 'i'";
		int count = 0;
		while (rs.next()) {
			count++;
		}
		movies.put(i, count);
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="header.jsp" />
	<div class="container">
		<div class="admin">
			<div class="row clearfix">
				<div class="col-md-12">
					<div class="admin-name"><%=username%>
						| <a href="login.jsp" onclick="logout()">Log Out</a>
					</div>
					<div class="add">
						<table class="table">
							<tr>
								<th>S.N.</th>
								<th>Cluster Name</th>
								<th>No. of movies</th>
							</tr>
							<%
									int cnt = 0;
									for (Integer i : movies.keySet()) {
								%>

							<tr>
								<td><%=cnt++%></td>
								<td><%=i%></td>
								<td><%=movies.get(i)%></td>
								</tr>
								<%} %>
</body>
</html>