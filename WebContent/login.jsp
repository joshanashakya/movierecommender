<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	if(session.getAttribute("userInfo") != null) {
		out.println(session.getAttribute("userInfo"));
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Movie Recommender System Login</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</head>
<body>
	<jsp:include page="header.jsp" />
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="login">
					<h4>Admin Login</h4>
					<span>
						<%
							if (request.getAttribute("message") != null)
								out.println(request.getAttribute("message"));
						%>
					</span><br>
					<form name="form" action="LoginController" method="post">
						<div class="form-group">
							<input type="text" name="username" class="form-control"
								placeholder="Username">
						</div>
						<div class="form-group">
							<input type="password" name="password" class="form-control"
								placeholder="Password">
						</div>
						<div class="form-group">
							<input type="submit" name="Login" class="btn btn-success"
								value="Login"> <input type="button" name="register"
								class="btn btn-success" value="Register" data-toggle="modal"
								data-target="#modal">
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="modal fade" id="modal" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div class="modal-body">
							<p>
							<h4>Inform super user for registration!!!</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp" />
</body>
</html>
