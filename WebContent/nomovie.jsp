<%@page contentType="text/html" pageEncoding="UTF-8" import = "java.sql.*, recommender.service.*, java.util.*"%>
<% Connection con = DatabaseConnection.getConnection();
	String sql = "select * from movie";
	PreparedStatement pstmt = con.prepareStatement(sql);
	ResultSet rs = pstmt.executeQuery();
	List<String> movieName = new ArrayList<String>();
	List<String> directorName = new ArrayList<String>();
	List<java.sql.Date> releaseDate = new ArrayList<java.sql.Date>();
	List<String> totalTime = new ArrayList<String>();
	List<String> mLanguage = new ArrayList<String>();
	List<String> budget = new ArrayList<String>();
	List<String> image = new ArrayList<String>();
	List<String> availableLink = new ArrayList<String>();
	while(rs.next()){
		movieName.add(rs.getString("MovieName"));
		directorName.add(rs.getString("DirectorName"));
		releaseDate.add(rs.getDate("ReleaseDate"));
		totalTime.add(rs.getString("TotalTime"));
		mLanguage.add(rs.getString("MLanguage"));
		budget.add(rs.getString("Budget"));
		image.add(rs.getString("ImagePath"));
		availableLink.add(rs.getString("AvailableLink"));
	}
	rs.close();
	pstmt.close();
	con.close();
	%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <title>Movie Recommendation </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?=$titles->description?>">
    <meta name="keywords" content="<?=$titles->keywords?>" />
    <meta name="author" content="">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="images/<?=$titles->logo?>">
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script type="text/javascript" src="js/tabs.js"></script>
	<script type="text/javascript" src="js/ajax.js"></script>
        <script> 
        $(document).ready(function(){
            $(".detail").hide();
            $("div.movie").click(function(){
                var id = $(this).attr('id');   
                id='detail'+id;
                $("#"+id).slideToggle("slow");
            });
        });
        
        </script>
        
        <script>
        $( document ).ready(function() {
          $('#search').keyup(function(){
           var valThis = $(this).val().toLowerCase();
            $('.main1>p').each(function(){
             var text = $(this).text().toLowerCase();
                (text.indexOf(valThis) == 0) ? $(this).show() : $(this).hide();            
           });
          });
        });
        </script>
        
        
       
        
   </head>
    <body>
        <%@include file="header.jsp" %>
        <div class="container">
            
                    <div class="main">
                        <div class="row clearfix">
                            
                            <div class="col-md-2">
                                
                            </div>
                            <div class="col-md-8" id="search">
                                <div class="search">
                                    
                                    <form name="form" role="form" method="post" action="search.jsp">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name = "search" placeholder="Find your movies .....">
                                        </div> 
                                    </form>
                                </div>
                                
                                
                                </div>
                            <div class="col-md-2">
                                
                            </div>
                        </div>
                        <div class="row clearfix">
                            
                            <div class="col-md-2">
                                
                            </div>
                             
                            <div class="col-md-8">
                                
                                <div class="sorry">
                                <p id="sorry">No search found !!!</p>
                                </div>
                            </div>    
                            <div class="col-md-2">
                                
                            </div>
                            </div>
                        <div class="main1">
                             <p class="rec">Movies</p>
                             <% for(int i = 0; i< 5; i++){%>
                            <div class="row clearfix">
                               
                                <div class="col-md-3">
                                    <div class="movie" id="<%=i%>">
                                        <img src="images/<%=image.get(i)%>" onclick="change();">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <p class="cap"><a href="search.jsp" class="a"><%=movieName.get(i) %></a></p>
                                    <p class = "cap">By <%=directorName.get(i) %>
                                    <p class="cap">Available on <a href="<%=availableLink.get(i)%>"><%=availableLink.get(i)%></a></p>
                                </div>
                                
                                
                                <div class="col-md-4">
                                    
                                </div>
                            </div>                          
                            
                            
                            <div class="row clearfix">
                                <div class="detail" id="detail<%=i%>">
                                    <p class="rec">Detail of <%=movieName.get(i) %></p>
                                <div class="col-md-4">
                                    
                                        <img src="images/<%=image.get(i)%>">
                                        
                                        
                                     
                                    </div>
                                
                                   <div class="col-md-5">                                 
                                                                               
                                    
                                    <p class = "cap">By :<%=directorName.get(i) %>
                                    
                                     <p class="cap">Release Date: <%=releaseDate.get(i)%></p> 
                                     <p class="cap">Total time: <%=totalTime.get(i)%></p> 
                                     <p class="cap">Language: <%=mLanguage.get(i)%></p> 
                                     <p class="cap">Budget: <%=budget.get(i)%></p> 
                                     <p class="cap">Available on : <a href="<%=availableLink.get(i)%>"><%=availableLink.get(i)%></a></p>
                                     
                                    </div>
                                </div>
                                </div>
                           
                            
                            
                            
                            
                            <%  } %>  
                            
                            
                            
                            
                        </div>
                            
                    </div>
             
       </div>
        <%@include file="footer.jsp" %>
    </body>
</html>
