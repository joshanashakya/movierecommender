package preprocessing;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.io.*;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

/**
 * This class lemmatizes the document.
 *
 */
public class Lemmatization {

	protected StanfordCoreNLP pipeline;

	public Lemmatization() {
		Properties props;
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		this.pipeline = new StanfordCoreNLP(props);
	}

	/**
	 * @param documentText
	 * @param filename
	 */
	public void lemmatize(String documentText, String filename) {
		List<String> lemmas = new LinkedList<String>();
		// Create an empty Annotation just with the given text
		Annotation document = new Annotation(documentText);
		// run all Annotators on this text
		this.pipeline.annotate(document);
		// Iterate over all of the sentences found
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			// Iterate over all tokens in a sentence
			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
				// Retrieve and add the lemma for each word into the
				// list of lemmas

				lemmas.add(token.get(LemmaAnnotation.class));
			}
		}
		// return lemmas;
		String lemmaString = "";
		for (String l : lemmas) {
			lemmaString += l + " ";
		}
		String name = filename.substring(62, filename.length() - 4) + ".txt";
		String oFile = "D:\\Extras\\lunaworkspace\\movierecommender\\WebContent\\files\\preprocessed\\"
				+ name;
		try (BufferedWriter bWrite = new BufferedWriter(new FileWriter(oFile))) {
			bWrite.write(lemmaString);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
