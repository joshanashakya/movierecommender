package preprocessing;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * This class is for reading files from local machine.
 *
 */
public class ReadFile {

	/**
	 * @param filename
	 * @return tokens of subtitle file
	 */
	public StringTokenizer getFileTokens(String filename) {
		String allLine = "";
		try (Scanner sRead = new Scanner(new FileReader(filename))) {
			// sRead.useDelimiter(Pattern.compile("[ \n\r\t,.;:?!'\"]+"));
			// sRead.useDelimiter("[,.;:?!'\"]+");
			// int cnt = 0;
			// while(cnt != 15){
			while (sRead.hasNextLine()) {
				String line = sRead.nextLine();
				if (isTimeCode(line) == true) {
					continue;
				} else if (line.equals("")) {
					continue;
				} else {
					allLine += line + " ";
				}
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		String lineLower = allLine.toLowerCase();
		StringTokenizer st = new StringTokenizer(lineLower,
				",.;?:!-><()[]#~`/\" ");
		// StringTokenizer st = new StringTokenizer(allLine, " ");
		return st;
		// return str.replaceAll("\\s+", " ");
	}

	/**
	 * It checks if the string line is timecode or not.
	 * 
	 * @param line
	 * @return true/false
	 */
	public static boolean isTimeCode(String line) {
		boolean flag = false;
		for (int i = 0; i <= 9; i++) {
			Integer num = (Integer) i;
			String str = num.toString();
			if (line.startsWith(str)) {
				flag = true;
				break;
			} else {
				continue;
			}
		}
		return flag;
	}

	/**
	 * It reads stopwords from stopword file.
	 * 
	 * @return stopwords
	 */
	public static String[] getStopwords() {
		String[] stopWords;
		String lineString = "";
		try (BufferedReader bReader = new BufferedReader(
				new FileReader(
						"D:\\Extras\\lunaworkspace\\movierecommender\\WebContent\\files\\stopwords.txt"))) {
			String line = null;
			while ((line = bReader.readLine()) != null) {
				lineString += line + " ";
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		stopWords = lineString.split(" ");
		return stopWords;
	}
}
