package preprocessing;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import recommender.service.DatabaseConnection;

/**
 * The class to perform file preprocessing.
 *
 */
public class Preprocessor {

	public void filePreprocess() {
		Connection con = DatabaseConnection.getConnection();
		String sql = "select * from Movie";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<String> subFiles = new LinkedList<String>();
		String filename;
		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				subFiles.add(rs.getString("SubtitlePath"));
			}
			for (int i = 0; i < subFiles.size(); i++) {
				filename = "D:\\Extras\\lunaworkspace\\movierecommender\\WebContent\\files\\srt\\"
						+ subFiles.get(i);
				ReadFile read = new ReadFile();
				StringTokenizer tokens = read.getFileTokens(filename);

				Stopword objEliminate = new Stopword();
				List<String> afterRemoval = objEliminate
						.getTokens(tokens);

				StringBuilder listString = new StringBuilder();
				for (String s : afterRemoval) {
					listString.append(s + " ");
				}
				Lemmatization slem = new Lemmatization();
				slem.lemmatize(listString.toString(), filename);
			}
		} catch (SQLException ex) {
			throw new Error(ex);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (con != null)
					con.close();
			} catch (SQLException ex) {
				throw new Error(ex);
			}
		}
	}
}
