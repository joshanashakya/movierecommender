package preprocessing;

import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * The class for removing stop words.
 *
 */
public class Stopword {
	/**
	 * @param stTokens
	 * @return tokens after removing stopwords
	 */
	public List<String> getTokens(StringTokenizer stTokens) {
		String[] stopWords = ReadFile.getStopwords();
		// String[] stringContent = breakContents(content);
		// String afterRemoval = "";
		// int count = 0;
		List<String> afterRemoval = new LinkedList<String>();
		while (stTokens.hasMoreTokens()) {
			String token = stTokens.nextToken();
			if (!isStopWord(token, stopWords)) {
				afterRemoval.add(token);
			} else {
				continue;
			}
		}
		return afterRemoval;
	}

	/**
	 * It breaks contents.
	 * 
	 * @param content
	 * @return
	 */
	public String[] getSplittedContents(String content) {
		String[] arrayContent;
		arrayContent = content.split(" ");
		return arrayContent;
	}

	/**
	 * It checks whether the word is stop word or not.
	 * 
	 * @param word
	 * @param stopWords
	 * @return
	 */
	public static boolean isStopWord(String word, String[] stopWords) {
		boolean flag = false;
		for (int i = 0; i < stopWords.length; i++) {
			if (compareWords(word, stopWords[i])) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	/**
	 * @param word
	 * @param stopword
	 * @return
	 */
	public static boolean compareWords(String word, String stopword) {
		boolean flag = false;
		// int i = word.compareToIgnoreCase(stopword);
		String lowerWord = word.toLowerCase();
		if (lowerWord.equals(stopword)) {
			flag = true;
		}
		return flag;
	}
}
