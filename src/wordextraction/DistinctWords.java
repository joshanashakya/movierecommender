package wordextraction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import recommender.service.DatabaseConnection;

/**
 * This class calculates distinct words among different documents.
 *
 */
public class DistinctWords {

	public void distinctCalculation() {
		String dirName = "D:\\Extras\\lunaworkspace\\movierecommender\\WebContent\\files\\preprocessed";
		List<String> preprocessedfile = new LinkedList<String>();
		Connection con = DatabaseConnection.getConnection();
		String sql = "select * from PreprocessedFile";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				preprocessedfile.add(rs.getString("PreprocessedFilePath"));
			}
			for (int i = 0; i < preprocessedfile.size(); i++) {
				String filename = dirName + "\\" + preprocessedfile.get(i);
				ReadWriteFiles read = new ReadWriteFiles();
				read.readFile(filename);
			}
		} catch (SQLException ex) {
			throw new Error(ex);
		}
	}
}
