package wordextraction;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * This class manages file handling.
 *
 */
public class ReadWriteFiles {

	public void readFile(String filename) {
		String line = null;
		try (Scanner bRead = new Scanner(new FileReader(filename))) {
			line = bRead.nextLine();
			StringTokenizer tokens = new StringTokenizer(line, " ");
			addDistinctWordList(tokens);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void addDistinctWordList(StringTokenizer tokens) {
		List<String> wordList = new LinkedList<String>();
		while (tokens.hasMoreTokens()) {
			String tmp = tokens.nextToken();
			if (!wordList.contains(tmp)) {
				wordList.add(tmp);
			}
		}
		addToFile(wordList);
	}

	public static void addToFile(List<String> wordList) {
		File file = new File(
				"D:\\Extras\\lunaworkspace\\movierecommender\\WebContent\\files\\distinctwords.txt");
		if (file.exists()) {
			appendWords(wordList, file);
		} else {

			try (BufferedWriter fWrite = new BufferedWriter(new FileWriter(
					file, true))) {
				for (String word : wordList) {
					fWrite.write(word);
					fWrite.newLine();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public static void appendWords(List<String> wordList, File file) {
		List<String> afileWords = new LinkedList<String>();
		List<String> distinctWords = new LinkedList<String>();
		try (BufferedReader bReader = new BufferedReader(new FileReader(file))) {
			String word = "";
			while ((word = bReader.readLine()) != null) {
				afileWords.add(word);
			}
			for (String a : wordList) {
				if (!afileWords.contains(a)) {
					distinctWords.add(a);
				}
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		try (BufferedWriter bWriter = new BufferedWriter(new FileWriter(file,
				true))) {
			for (String a : distinctWords) {
				bWriter.write(a);
				bWriter.newLine();
			}
		} catch (IOException ex) {

		}
	}

	public static void readwriteAscendFile(String filename) {
		List<String> distWords = new LinkedList<String>();
		try (BufferedReader reader = new BufferedReader(
				new FileReader(filename));
				BufferedWriter writer = new BufferedWriter(
						new FileWriter(
								"D:\\Extras\\lunaworkspace\\movierecommender\\WebContent\\files\\ascend.txt"))) {
			String line;
			while ((line = reader.readLine()) != null) {
				distWords.add(line);
			}
			Collections.sort(distWords);
			for (String word : distWords) {
				writer.write(word);
				writer.newLine();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
