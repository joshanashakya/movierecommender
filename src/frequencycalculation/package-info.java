/**
 * This package contains classes for calculating and storing frequency of documents in files.
 *
 */
package frequencycalculation;