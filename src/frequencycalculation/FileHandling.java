package frequencycalculation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Formatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * This class reads from and writes to necessary files.
 *
 */
public class FileHandling {
	/**
	 * @param filename
	 * @return text
	 */
	public List<String> getText(String filename) {
		String textline = "";
		List<String> text = new LinkedList<String>();
		try (Scanner read = new Scanner(new FileReader(filename))) {
			textline = read.nextLine();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		StringTokenizer tokens = new StringTokenizer(textline, " ");
		while (tokens.hasMoreTokens()) {
			text.add(tokens.nextToken());
		}
		return text;
	}

	/**
	 * @param file
	 * @return ascended text
	 */
	public static List<String> getAscendedText(String file) {
		List<String> words = new LinkedList<String>();
		try (BufferedReader read = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = read.readLine()) != null) {
				words.add(line);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return words;
	}

	/**
	 * It writes frequency to file.
	 * @param ascendwords
	 * @param filewords
	 * @param fname
	 */
	public void writeFrequency(List<String> ascendwords,
			List<String> filewords, String fname) {
		// Set<String> unique = new HashSet<String>(ascendwords);
		try (Formatter writer = new Formatter(new FileWriter(fname))) {
			for (String key : ascendwords) {
				int count = Collections.frequency(filewords, key);
				// writer.write(count);
				// writer.newLine();
				writer.format("%d ", count);
				// System.out.println(count);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
