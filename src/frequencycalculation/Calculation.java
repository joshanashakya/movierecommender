package frequencycalculation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import recommender.service.DatabaseConnection;

/**
 * This is the class for frequency calculation.
 *
 */
public class Calculation {

	/**
	 * It calculates frequency.
	 */
	public void calculate() {
		String file = "D:\\Extras\\lunaworkspace\\movierecommender\\WebContent\\files\\ascend.txt";
		String dirname = "D:\\Extras\\lunaworkspace\\movierecommender\\WebContent";
		List<String> preprocessedfile = new LinkedList<String>();
		Connection con = DatabaseConnection.getConnection();
		String sql = "select * from PreprocessedFile";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				preprocessedfile.add(rs.getString("PreprocessedFilePath"));
			}
		} catch (SQLException ex) {
			throw new Error(ex);
		}
		for (int i = 0; i < preprocessedfile.size(); i++) {
			String filename = "D:\\Extras\\lunaworkspace\\movierecommender\\WebContent\\files\\preprocessed\\"
					+ preprocessedfile.get(i);
			String fname = "D:\\Extras\\lunaworkspace\\movierecommender\\WebContent\\files\\term-frequency\\tf-"
					+ filename.substring(71, filename.length() - 4) + ".txt";
			FileHandling rf = new FileHandling();
			List<String> filewords = rf.getText(filename);
			List<String> ascendwords = FileHandling.getAscendedText(file);
			rf.writeFrequency(ascendwords, filewords, fname);
		}
	}
}
