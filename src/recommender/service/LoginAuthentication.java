package recommender.service;

import java.sql.*;

public class LoginAuthentication {
	public boolean authenticate(String username, String password) {
		Connection con = DatabaseConnection.getConnection();
		String sql = "select name, username, password from admin";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String name, uname, pword;
		boolean flag = false;
		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				name = rs.getString("name");
				uname = rs.getString("username");
				pword = rs.getString("password");
				if (username.equals(uname) && password.equals(pword)) {
					flag = true;
				}
			}
		} catch (SQLException ex) {
			throw new Error(ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException ex) {
				throw new Error(ex);
			}
		}
		return flag;
	}
}
