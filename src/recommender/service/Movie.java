package recommender.service;

import java.sql.Date;

/**
 * This class contains properties of a movie.
 *
 */
public class Movie {
	private String movieName;
	private String directorName;
	private Date releaseDate;
	private String totalTime;
	private String mLanguage;
	private String budget;
	private String image;
	private String availableLink;

	public Movie(String movieName, String directorName, Date releaseDate,
			String totalTime, String mLanguage, String budget, String image,
			String availableLink) {
		this.movieName = movieName;
		this.directorName = directorName;
		this.releaseDate = releaseDate;
		this.totalTime = totalTime;
		this.mLanguage = mLanguage;
		this.budget = budget;
		this.image = image;
		this.availableLink = availableLink;
	}

	public String getMovieName() {
		return movieName;
	}

	public String getDirectorName() {
		return directorName;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public String getTotalTime() {
		return totalTime;
	}

	public String getmLanguage() {
		return mLanguage;
	}

	public String getBudget() {
		return budget;
	}

	public String getImage() {
		return image;
	}

	public String getAvailableLink() {
		return availableLink;
	}
}
