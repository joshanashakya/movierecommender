package recommender.service;

import java.sql.*;

/**
 * This class provides database connection.
 *
 */
public class DatabaseConnection {
	
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection(
					"jdbc:mysql://localhost/RecommenderSystem", "root", "root");
		} catch (Exception ex) {
			throw new Error(ex);
		}
	}

}