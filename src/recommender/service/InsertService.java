package recommender.service;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import clustering.Cluster;

/**
 * This class provides service for adding data to the database.
 *
 */
public class InsertService {

	public int adminInsert(String name, String phoneno, String emailid,
			String username, String password) {
		Connection con = DatabaseConnection.getConnection();
		String sql = "insert into Admin(Name, PhoneNo, EmailId, Username, Password) values (?, ?, ?, ?, ?)";
		PreparedStatement pstmt = null;
		int count;
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, name);
			pstmt.setString(2, phoneno);
			pstmt.setString(3, emailid);
			pstmt.setString(4, username);
			pstmt.setString(5, password);
			count = pstmt.executeUpdate();

		} catch (SQLException ex) {
			throw new Error(ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null)
					con.close();
			} catch (SQLException ex) {
				throw new Error(ex);
			}
		}
		return count;
	}

	public int movieInsert(String mName, String avlLink, String dirName,
			Date rDate, String time, String language, String budget,
			String image, String subpath) {
		Connection con = DatabaseConnection.getConnection();
		String sql = "insert into movie(MovieName, AvailableLink, DirectorName, ReleaseDate, TotalTime, MLanguage, Budget, ImagePath, SubtitlePath) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement pstmt = null;
		int count;
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, mName);
			pstmt.setString(2, avlLink);
			pstmt.setString(3, dirName);
			pstmt.setDate(4, rDate);
			pstmt.setString(5, time);
			pstmt.setString(6, language);
			pstmt.setString(7, budget);
			pstmt.setString(8, image);
			pstmt.setString(9, subpath);

			count = pstmt.executeUpdate();

		} catch (SQLException ex) {
			throw new Error(ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null)
					con.close();
			} catch (SQLException ex) {
				throw new Error(ex);
			}
		}
		return count;
	}

	public void insertClusterId(Cluster cluster) {
		Connection con = DatabaseConnection.getConnection();
		String sql = "select * from cluster where clusterName = +'"cluster.getCname()"'+";
	}

	public void insertCluster(List<Cluster> clusters) {
		Connection con = DatabaseConnection.getConnection();
		String sql = "insert into cluster(clusterName) values(?)";
		try {
			for (Cluster cluster : clusters) {
				PreparedStatement pstmt = con.prepareStatement(sql);
				pstmt.setString(1, cluster.getCname());
				pstmt.executeUpdate();
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
}
