package recommender.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import recommender.service.LoginAuthentication;

/**
 * Servlet implementation class checking if admin is valid
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username = request.getParameter("username").trim();
		String password = request.getParameter("password").trim();

		LoginAuthentication objLogin = new LoginAuthentication();
		boolean flag = objLogin.authenticate(username, password);
		if (flag) {
			HttpSession session = request.getSession(true);
			session.setAttribute("userInfo", username);
			response.sendRedirect("admin.jsp");
			return;
		} else {
			request.setAttribute("message", "Invalid username or password");
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
			return;
		}
	}

}
