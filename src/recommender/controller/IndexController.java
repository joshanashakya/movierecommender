package recommender.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import recommender.service.DatabaseConnection;
import recommender.service.Movie;

/**
 * Servlet implementation class IndexController
 */
@WebServlet("/IndexController")
public class IndexController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Connection con = DatabaseConnection.getConnection();
		String sql = "select * from movie";
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		List<Movie> movieData = new ArrayList<Movie>();
		PrintWriter out = response.getWriter();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				movieData.add(new Movie(rs.getString("movieName"), rs
						.getString("directorName"), rs.getDate("releaseDate"),
						rs.getString("totalTime"), rs.getString("mLanguage"),
						rs.getString("budget"), rs.getString("imagepath"), rs
								.getString("availableLink")));
			}
		} catch (SQLException ex) {
			throw new Error(ex);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (con != null)
					con.close();
			} catch (SQLException ex) {
				throw new Error(ex);
			}
		}
		request.setAttribute("moviedata", movieData);
		RequestDispatcher rd = request.getRequestDispatcher("index2.jsp");
		if (rd != null)
			rd.forward(request, response);
	}

}
