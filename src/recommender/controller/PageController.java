package recommender.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class for redirecting pages.
 */
@WebServlet("/PageController")
public class PageController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if (request.getParameter("update") != null) {
			System.out.println(request.getParameter("id"));
		}
		if (request.getParameter("add") != null) {
			RequestDispatcher rd = request
					.getRequestDispatcher("moviedetail.jsp");
			rd.forward(request, response);
		} else if (request.getParameter("cluster") != null) {
			RequestDispatcher rd = request
					.getRequestDispatcher("ClusterController");
			rd.forward(request, response);

		}
	}
}
