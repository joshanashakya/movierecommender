package recommender.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import preprocessing.Preprocessor;
import wordextraction.Ascender;
import wordextraction.DistinctWords;
import clustering.Clustering;
import frequencycalculation.Calculation;

/**
 * Servlet implementation class for clustering documents.
 *
 */
@WebServlet("/ClusterController")
public class ClusterController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			// Preprocessor objLemmatize = new Preprocessor();
			// objLemmatize.filePreprocess();
			//
			// DistinctWords objMake = new DistinctWords();
			// objMake.distinctCalculation();
			//
			// Ascender objAscend = new Ascender();
			// objAscend.ascend();
			//
			// Calculation objFreq = new Calculation();
			// objFreq.calculate();

			Clustering objTest = new Clustering();
			objTest.cluster();

			RequestDispatcher rd = request.getRequestDispatcher("admin.jsp");
			rd.forward(request, response);
		} catch (Exception ex) {
			RequestDispatcher rd = request.getRequestDispatcher("error.jsp");
			rd.forward(request, response);
		}
	}
}
