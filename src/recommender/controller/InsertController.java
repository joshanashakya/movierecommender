package recommender.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import recommender.service.InsertService;

/**
 * Servlet implementation class for adding movie in database.
 */
@WebServlet("/InsertController")
public class InsertController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if (request.getParameter("cancel") != null) {
			PrintWriter out = response.getWriter();

			out.println("rdate");
			RequestDispatcher rd = request.getRequestDispatcher("admin.jsp");
			rd.forward(request, response);
		} else if (request.getParameter("add") != null) {
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String mName = request.getParameter("mName");
			String avlLink = request.getParameter("avlLink");
			String dirName = request.getParameter("dirName");
			String rdateString = request.getParameter("rDate");
			java.sql.Date rdate = java.sql.Date.valueOf(rdateString);
			String time = request.getParameter("time");
			String language = request.getParameter("language");
			String budget = request.getParameter("budget");
			String image = request.getParameter("image");
			String subpath = request.getParameter("subpath");

			InsertService objInsert = new InsertService();
			int cnt = objInsert.movieInsert(mName, avlLink, dirName, rdate,
					time, language, budget, image, subpath);
			if (cnt == 1) {
				response.sendRedirect("admin.jsp");
			} else {
				response.sendRedirect("moviedetail.jsp");
			}

		}
	}

}
