package recommender.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import recommender.service.InsertService;

/**
 * Servlet implementation class RegistrationController
 */
@WebServlet("/RegistrationController")
public class RegistrationController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String phoneno = request.getParameter("phonene");
		String emailid = request.getParameter("emailid");
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		InsertService objInsert = new InsertService();
		int cnt = objInsert.adminInsert(name, phoneno, emailid, username,
				password);

		/*
		 * PrintWriter out = response.getWriter(); out.println(cnt);
		 * out.close();
		 */

		if (cnt == 1) {
			response.sendRedirect("admin.jsp");
			return;
		} else {
			response.sendRedirect("adminregister.jsp");
			return;
		}
	}

}
