package clustering;

import java.util.*;

/**
 * The class for computing unique random number.
 *
 */
public class NumberGenerator {

	public static HashSet<Integer> getRandomNumber(int numCluster, int numDocs) {
		int rnd;
		Random rand = new Random();
		HashSet<Integer> randno = new HashSet<Integer>(numCluster);
		while (randno.size() != numCluster) {
			while (randno.add(rand.nextInt(numDocs)) != true)
				;
		}
		assert randno.size() == numCluster;
		return randno;
	}
}
