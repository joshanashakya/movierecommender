package clustering;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

/**
 * This class implements K-Means clustering algorithm.
 *
 */
public class Clusterer {

	private List<String> initialClusterAssignments = null;

	/**
	 * It clusters the documents
	 * 
	 * @param collection
	 * @return clusters
	 */
	public List<Cluster> getClusters(Collection collection) {
		int numDocs = collection.size();
		int numClusters = 3;
		initialClusterAssignments = new LinkedList<String>();
		HashSet<Integer> randno = new HashSet<Integer>();
		randno = NumberGenerator.getRandomNumber(numClusters, numDocs);
		Iterator it = randno.iterator();
		for (int i = 0; i < numClusters; i++) {
			int id = (int) it.next();
			initialClusterAssignments.add(collection.getDocumentNameAt(id));
		}
		// build initial cluster
		List<Cluster> clusters = new ArrayList<Cluster>();
		for (int i = 0; i < numClusters; i++) {
			Cluster cluster = new Cluster("C" + i);
			cluster.addDocument(initialClusterAssignments.get(i),
					collection.getDocument(initialClusterAssignments.get(i)));
			clusters.add(cluster);
		}
		List<Cluster> prevClusters = new ArrayList<Cluster>();
		for (;;) {
			List[] centroids = new List[numClusters];
			for (int i = 0; i < numClusters; i++) {
				LinkedList<Double> centroid = new LinkedList<Double>();
				centroid = clusters.get(i).getCentroid();
				centroids[i] = centroid;
			}
			for (int i = 0; i < numDocs; i++) {
				int bestCluster = 0;
				double maxSimilarity = Double.MIN_VALUE;
				LinkedList<Integer> document = collection.getDocumentAt(i);
				String docName = collection.getDocumentNameAt(i);
				for (int j = 0; j < numClusters; j++) {
					double similarity = clusters.get(j).getSimilarity(document);
					if (similarity > maxSimilarity) {
						bestCluster = j;
						maxSimilarity = similarity;
					}
				}
				for (Cluster cluster : clusters) {
					if (cluster.getDocument(docName) != null) {
						cluster.removeDocument(docName);
					}
				}
				clusters.get(bestCluster).addDocument(docName, document);
			}
			// System.out.println("..Intermediate clusters: ");
			// for (Cluster c : clusters) {
			// System.out.println(c.toString());
			// }
			// Check for termination -- minimal or no change to the assignment
			// of documents to clusters.
			if (CollectionUtils.isEqualCollection(clusters, prevClusters)) {
				break;
			}
			prevClusters.clear();
			prevClusters.addAll(clusters);
		}
		// System.out.println("..Final clusters: "); for(Cluster c : clusters){
		// System.out.println(c.toString()); }
		return clusters;
	}
	// Return list of clusters
}
