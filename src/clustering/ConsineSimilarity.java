package clustering;

import java.util.LinkedList;

/**
 * This is the class for computing cosine similarity of document vectors.
 *
 */
public class ConsineSimilarity {
	
	/**
	 * It calculates cosine similarity.
	 * @param centroid
	 * @param doc
	 * @return similarity score
	 */
	public double getSimilarityScore(LinkedList<Double> centroid, LinkedList<Integer> doc) {
		LinkedList<Double> tf_idfcentroid = new LinkedList<Double>();
		LinkedList<Double> tf_idfdoc = new LinkedList<Double>();
		tf_idfcentroid = getDoubleTF(centroid);
		tf_idfdoc = getIntegerTF(doc);
		double dotproduct = 0;
		for (int i = 0; i < tf_idfcentroid.size(); i++) {
			double multiplication = tf_idfcentroid.get(i) * tf_idfdoc.get(i);
			dotproduct += multiplication;
		}
		// System.out.println(dotproduct);
		double tf_idfcentroidMagnitude = getMagnitude(tf_idfcentroid);
		double tf_idfdocMagnitufe = getMagnitude(tf_idfdoc);
		double cosineSimilarity = dotproduct
				/ (tf_idfcentroidMagnitude * tf_idfdocMagnitufe);
		return cosineSimilarity;
	}

	/**
	 * @param tf_idfdoc
	 * @return magnitude
	 */
	public static double getMagnitude(LinkedList<Double> tf_idfdoc) {
		double sum = 0;
		for (double i : tf_idfdoc) {
			double sq = i * i;
			sum += sq;
		}
		double magnitude = Math.sqrt((double) sum);
		return magnitude;
	}

	/**
	 * It calculates term frequency of documents having integer frequency.
	 * @param frequency
	 * @return term frequency
	 */
	public static LinkedList<Double> getIntegerTF(
			LinkedList<Integer> frequency) {
		LinkedList<Double> tf_idf = new LinkedList<Double>();
		int total = 0;
		for (Integer freq : frequency) {
			total += freq;
		}
		for (Integer freq : frequency) {
			double termfreq = (double) ((double) freq / (double) total);
			double term_inverseFreq = termfreq * 1;
			tf_idf.add(term_inverseFreq);
		}
		return tf_idf;
	}

	/**
	 * It calculates term frequency of documents having double frequency.
	 * @param frequency
	 * @return term frequency
	 */
	public static LinkedList<Double> getDoubleTF(
			LinkedList<Double> frequency) {
		LinkedList<Double> tf_idf = new LinkedList<Double>();
		double total = 0;
		for (Double freq : frequency) {
			total += freq;
		}
		for (double freq : frequency) {
			double termfreq = (double) ((double) freq / (double) total);
			double term_inverseFreq = termfreq * 1;
			tf_idf.add(term_inverseFreq);
		}
		return tf_idf;
	}

}
