/**
 * This package contains classes for clustering subtitle documents and those required for clustering.
 *
 */
package clustering;