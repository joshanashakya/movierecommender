package clustering;

import java.util.*;
import java.io.*;

import recommender.service.InsertService;

/**
 * This class performs clustering of document.
 *
 */
public class Clustering {

	private LinkedList<LinkedList<Integer>> vector = new LinkedList<LinkedList<Integer>>();
	private List<String> documentNames = new ArrayList<String>();
	private static Collection documentCollection;

	/**
	 * Initial set up for clustering document.
	 */
	public void set() {
		Map<String, LinkedList<Integer>> documents = new LinkedHashMap<String, LinkedList<Integer>>();
		String dirpath = "D:\\Extras\\lunaworkspace\\movierecommender\\WebContent\\files\\term-frequency";
		File dir = new File(dirpath);
		File[] listoffiles = dir.listFiles();

		for (int i = 0; i < listoffiles.length; i++) {
			String filename = listoffiles[i].getName();
			String filepath = dirpath + "\\" + filename;
			LinkedList<Integer> frequency = new LinkedList<Integer>();
			documentNames.add(filename);
			try (Scanner sreader = new Scanner(new FileReader(filepath))) {
				while (sreader.hasNext()) {
					frequency.add(sreader.nextInt());
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			documents.put(filepath, frequency);
			vector.add(frequency);
		}
		documentCollection = new Collection(vector, documentNames);
	}

	/**
	 * The method to get clusters of documents. 
	 */
	public void cluster() {
		Clustering ct = new Clustering();
		ct.set();
		Clusterer clusterer = new Clusterer();
		List<Cluster> clusters = clusterer.getClusters(documentCollection);
		System.out.println("=== Clusters from K-Means algorithm ===");
		InsertService objInsert = new InsertService();
		objInsert.insertCluster(clusters);
		for (Cluster cluster : clusters) {
			objInsert.insertClusterId(cluster);
		}
	}
}
