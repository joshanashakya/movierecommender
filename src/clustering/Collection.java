package clustering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This class defines the document collection.
 *
 */
public class Collection {
	private LinkedList<LinkedList<Integer>> vector;
	private Map<String, LinkedList<Integer>> documentMap;
	private List<String> documentNames;

	public Collection(LinkedList<LinkedList<Integer>> vector,
			List<String> docNames) {
		int position = 0;
		this.vector = vector;
		this.documentMap = new HashMap<String, LinkedList<Integer>>();
		this.documentNames = new ArrayList<String>();
		for (String documentName : docNames) {
			documentMap.put(documentName, vector.get(position));
			documentNames.add(documentName);
			position++;
		}
	}

	public int size() {
		return documentMap.keySet().size();
	}

	public List<String> getDocumentNames() {
		return documentNames;
	}

	public String getDocumentNameAt(int position) {
		return documentNames.get(position);
	}

	public LinkedList<Integer> getDocumentAt(int position) {
		return documentMap.get(documentNames.get(position));
	}

	public LinkedList<Integer> getDocument(String documentName) {
		return documentMap.get(documentName);
	}

	public void shuffle() {
		Collections.shuffle(documentNames);
	}
}
