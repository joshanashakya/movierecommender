package clustering;

import java.util.*;

/**
 * This class defines the single cluster of document/s.
 *
 */
public class Cluster {
	private String cname;
	private Map<String, LinkedList<Integer>> docs = new LinkedHashMap<String, LinkedList<Integer>>();
	private List<String> docNames = new LinkedList<String>();
	private LinkedList<Double> newCentroid = null;

	public Cluster(String cname) {
		super();
		this.cname = cname;
	}

	public String getCname() {
		return this.cname;
	}

	public Set<String> getDocumentNames() {
		return docs.keySet();
	}

	public String getDocumentName(int pos) {
		return docNames.get(pos);
	}

	public LinkedList<Integer> getDocument(String documentName) {
		return docs.get(documentName);
	}

	public LinkedList<Integer> getDocument(int pos) {
		return docs.get(docNames.get(pos));
	}

	/**
	 * It adds new document to the cluster.
	 * 
	 * @param docName
	 *            is name of document.
	 * @param docVector
	 *            is vector of document.
	 */
	public void addDocument(String docName, LinkedList<Integer> docVector) {
		docs.put(docName, docVector);
		docNames.add(docName);
		// System.out.println("...." + cname + " += " + docName);
	}

	/**
	 * It removes the document from the cluster.
	 * 
	 * @param docName
	 *            is name of document.
	 */
	public void removeDocument(String docName) {
		docs.remove(docName);
		docNames.remove(docName);
		// System.out.println("...." + cname + " -= " + docName);
	}

	/**
	 * @return centroid.
	 */
	public LinkedList<Double> getCentroid() {
		newCentroid = new LinkedList<Double>();
		if (docs.size() == 0) {
			return null;
		}
		LinkedList<Integer> d = docs.get(docNames.get(0));
		LinkedList<Integer> centroid = new LinkedList<Integer>();
		centroid = d;
		for (String docName : docs.keySet()) {
			LinkedList<Integer> link = docs.get(docName);
			centroid = getTotal(centroid, link);
		}
		for (Integer val : centroid) {
			double mean = (double) ((double) val / (double) docs.size());
			newCentroid.add(mean);
		}
		return newCentroid;
	}

	/**
	 * @param centroid
	 * @param link
	 * @return total
	 */
	public static LinkedList<Integer> getTotal(LinkedList<Integer> centroid,
			LinkedList<Integer> link) {
		LinkedList<Integer> total = new LinkedList<Integer>();
		for (int i = 0; i < centroid.size(); i++) {
			int plus = centroid.get(i) + link.get(i);
			total.add(plus);
		}
		return total;
	}

	/**
	 * It calculates the similarity of document to the cluster.
	 * 
	 * @param doc
	 * @return similarity score.
	 */
	public double getSimilarity(LinkedList<Integer> doc) {
		if (newCentroid != null) {
			ConsineSimilarity cal = new ConsineSimilarity();
			return cal.getSimilarityScore(newCentroid, doc);
		}
		return 0.0D;
	}

	@Override
	public String toString() {
		return cname + ":" + docs.keySet().toString();
	}
}
